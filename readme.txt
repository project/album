Description
--------------------
Personal image albums.

Prerequisites
--------------------
- folksonomy.module from Contrib
- image.module from http://cvs.drupal.org/viewcvs/contrib/contributions/sandbox/walkah/image/. This module will likely be included in 4.6 drupal.

Tech Notes
--------------------
Each image is a node. Albums comprise of all images within the same realm and with the same "tag". Realm and tag are components of the folksonomy module. For the purposes of this module, each person has his own "realm" where his tagspace is independant of all others.

Images can have more than one entry im the folksonomy table and thus appear in many different albums (think "tags" in Flickr). No UI exposes this yet.

To Do
---------------------
- slideshow
- expose more power of folksonomy
- allow for zip file uploading of albums
- get image captions from IPTC headers
- show images with same tag across all users
- block with recent images


Author
---------------------
Moshe Weitzman <weitzman AT tejasa DOT com>

